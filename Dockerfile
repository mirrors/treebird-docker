# Production container: only core debian and treebird FCGI deps
FROM debian:testing

RUN apt update && apt upgrade -y
RUN apt install -y libcurl3-gnutls libfcgi0ldbl fcgiwrap

WORKDIR treebird
COPY treebird-fcgi /usr/local/bin/treebird

CMD fcgiwrap -c 10 -f -s tcp:0.0.0.0:4008
