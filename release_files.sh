#!/bin/bash
# script to call the release container since its ugly in the drone yaml
export DRONE_REPO_NAME
export DRONE_REPO_OWNER
export PLUGIN_API_KEY

docker run \
    --rm \
    -e DRONE_BUILD_EVENT=tag \
    -e PLUGIN_BASE_URL=https://git.freecumextremist.com/ \
    -e PLUGIN_FILES=release/* \
    -e PLUGIN_API_KEY \
    -e DRONE_REPO_OWNER \
    -e DRONE_REPO_NAME \
    -v $(pwd):$(pwd) -w $(pwd) plugins/gitea-release
