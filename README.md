Containerized Treebird FE Build
========================================
This git repo contains files needed to build a docker container for
Nekobit's `Treebird` C-FCGI based front end for [Pleroma](https://pleroma.social/).

## Requirements - Build

- `docker` (or `docker.io` on older distros)
- bash
- git

## Requirements - Run

- `docker` (or `docker.io` on older distros)
- `nginx` (or another FCGI capable web server)
- `bash` (only for commands in the instructions. If you mentally translate you dont need it)

## Installing
To install you can either build the container yourself or grab the latest from
the Gitea release tab. Here I'm going to assume the latter.

1. On your server download the `treebird-latest` and `treebird-latest-static-files` tars

2. If you don't have it already, install docker (`apt install docker` or `docker.io` for old debian)
   NOTE: for other distros obviously this will differ so find it in your package manager

3. Load the image

```
sudo docker load -i treebird-latest.tar.gz
```

4. Add an unprivileged treebrid user and group to the host (this is needed to avoid uid/gid conflicts)

```
sudo useradd -r -s /bin/false -c 'Treebird FE user' -U treebird
```

5. Run the container as below to start in daemon mode (`-d`) on port 4008 as user:group treebird:treebird

```
docker run \
    --rm \
    --name treebird \
    -p 127.0.0.1:4008:4008 \
	-u "`id -u treebird`:`id -g treebird`" \
    -e TREEBIRD_CFG_INSTANCE_URL="https://my.instance.domain/" \
    -d "treebird:latest"
```

You can verify this started by checking `docker ps`
This should remain up until either the system is rebooted or the docker
daemon is restarted (or you can manually stop with the docker stop command).

To have it auto start on reboot you need to either add to your init system
or manage with a docker wrapper like docker-compose or etc.

6. Add a web folder for the static files

```
sudo mkdir -p /var/www/treebird
sudo chmod a+r /var/www/treebird
```

7. Extract static files to web folder

```
tar -xzf treebird-static-latest.tar.gz
sudo cp -r treebird-latest/* /var/www/treebird
```

8. Setup your Nginx Configuration (other servers idk for now)

```
# In case the folders do not exist yet, do the following
sudo mkdir -p /etc/nginx/sites-{available,enabled}

# If you needed the above, ensure you also you have `include sites-enabled/*;`
# in the http section of `/etc/nginx/nginx.conf`

# First edit treebird.nginx to replace treebird.example.com with your treebird hostname
sudo cp treebird.nginx /etc/nginx/sites-available/treebird.nginx
sudo ln -s /etc/nginx/{sites-available,sites-enabled}/treebird.nginx

# Activate SSL for domain (the usual... select domain and etc.)
sudo certbot --nginx

# Start nginx
sudo systemctl start nginx
```

It should now be running on the domain you configured.

## Updating
This is process is similar to the install, but without the need for config changes.

1. Download the latest tarballs from the release tab again.

2. Stop the running container and remove the old image

```
docker stop treebird
docker rmi treebird:latest
```

3. Load the new one

```
docker load -i treebird-latest-HASH.tar.gz
```

4. Start the container again

```
docker run \
    --rm \
    --name treebird \
    -p 127.0.0.1:4008:4008 \
	-u "`id -u treebird`:`id -g treebird`" \
    -e TREEBIRD_CFG_INSTANCE_URL="https://my.instance.domain/" \
    -d "treebird:latest"
```

5. Extract and update static files

```
tar -xzf treebird-static-files-HASH.tar.gz
sudo cp -rf treebird-latest/* /var/www/treebird
```

Your install should now be running and up to date

## Building
Building the container should be as simple as running `build_container.sh` and
waiting for the build to complete.

	./build_container.sh [tag (optional, defaults to latest)]

## Running
The `run_container.sh` script will start the `Treebird` FCGI container on port 4008
ready to receive requests from `nginx` or whatever FCGI capable http server you have.

	# NOTE: The instance url MUST contain the trailing /
	./run_container.sh [instance, e.g. "https://sleepy.cafe/"] [tag (optional, latest)]

### Running - Local Nginx
For local use and testing an `nginx` `Dockerfile` is provided in the `nginx-test` folder.
To use it, after building the `Treebird` container, do the following.

	cd nginx-test
	./build_container.sh [tag (optional, defaults to latest)]

Then to run it just run `run_container.sh`. This will *not* launch with the `-d`
option so it will close when you close the terminal (since this is only for testing).

	./run_container.sh [tag (optional, defaults to latest)]

From here, you can open your preferred web browser to `http://localhost/` and `Treebird`
should appear.
