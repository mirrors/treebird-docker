#!/bin/sh
# Builds both the 'build' and release containers. The build container is used to
# compile the code and is kept independently so it can actually be run for testing
# and debugging, but only the release image should be run on production servers.
release=${1:-latest}
outdir="treebird-${release}"

git submodule init
git submodule update

build_image="treebird:${release}-build"
docker build -t "${build_image}" -f Dockerfile.build .

[ $? -eq 0 ] || (exit 1 && echo build failed!)

[ -d "${outdir}" ] || mkdir "${outdir}"

static_files="/usr/local/share/treebird/dist/."
fcgi_bin_pth="/usr/local/bin/treebird"

docker run --rm --name "${outdir}-tmp" -d "${build_image}"
docker cp "${outdir}-tmp:${static_files}" "${outdir}/"
docker cp "${outdir}-tmp:${fcgi_bin_pth}" "${outdir}/"
docker stop "${outdir}-tmp"

[ $? -eq 0 ] || (exit 1 && echo failed to extract to outdir)

cp "${outdir}/treebird" treebird-fcgi
docker build -t "treebird:${release}" .
rm treebird-fcgi

[ $? -eq 0 ] || (exit 1 && echo failed to build release image)
