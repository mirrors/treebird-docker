#!/bin/sh
# builds a quick improvised nginx container to run treebird locally
release=${1:-latest}

[ ! -d to-webserv ] || rm -vr to-webserv

mkdir to-webserv
cp -r "../treebird-${release}/." to-webserv

docker build -t treebird-nginx-test .
