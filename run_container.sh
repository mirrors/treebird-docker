#!/bin/sh
# runs the treebird image as a docker daemon
instance=${1:-https://my.instance.domain/}
docker_tag=${2:-treebird:latest}
docker_user=${3:-treebird}

# NOTE: if you don't have a user you should make one for security

# IP and port to bind on (only use localhost 127.*.*.* for security reasons...)
host_bind_addr="127.0.0.1:4008"

docker run \
    --rm \
    --name treebird \
    -p "${host_bind_addr}:4008" \
    -u "`id -u ${docker_user}`:`id -g ${docker_user}`" \
    -e TREEBIRD_CFG_INSTANCE_URL="${instance}" \
    -d "${docker_tag}"
